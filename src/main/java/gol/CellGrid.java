package main.java.gol;

import java.io.*;
import java.util.Random;
import java.util.Vector;

/**
 * The Class CellGrid.
 */
public class CellGrid {

	/**
	 * The grid.
	 */
	private main.java.gol.Cell[][] grid;

	/**
	 * The cols.
	 */
	private int cols;

	/**
	 * The rows.
	 */
	private int rows;

	/**
	 * The generation.
	 */
	private int generation;

	private static final int[][] NEIGHBOURS = {
			{-1, -1}, {-1, 0}, {-1, +1},
			{0, -1},            {0, +1},
			{+1, -1}, {+1, 0}, {+1, +1}};

	/**
	 * The generation history.
	 */
	public void SaveGenerationHistory() {
		File f = new File("fields/history.txt");
		saveGrid(f);
	}

	/**
	 * Instantiates a new cell grid.
	 */
	public CellGrid() {
	}

	/**
	 * Instantiates a new cell grid.
	 *
	 * @param cols the cols
	 * @param rows the rows
	 */
	public CellGrid(int cols, int rows) {
		this.initGrid(cols, rows);
	}

	/**
	 * Sets the cell state.
	 *
	 * @param col       the col
	 * @param row       the row
	 * @param cellValue the cell value
	 */
	public void setCellState(int col, int row, boolean cellValue) {
		this.grid[col][row].setState(cellValue);
	}

	/**
	 * Gets the cell state.
	 *
	 * @param col the col
	 * @param row the row
	 * @return the cell state
	 */
	public boolean getCellState(int col, int row) {
		return this.grid[col][row].getState();
	}

	/**
	 * Sets the cols.
	 *
	 * @param cols the new cols
	 */
	private void setCols(int cols) {
		this.cols = cols;
	}

	/**
	 * Gets the cols.
	 *
	 * @return the cols
	 */
	public int getCols() {
		return this.cols;
	}

	/**
	 * Sets the rows.
	 *
	 * @param rows the new rows
	 */
	private void setRows(int rows) {
		this.rows = rows;
	}

	/**
	 * Gets the rows.
	 *
	 * @return the rows
	 */
	public int getRows() {
		return this.rows;
	}

	/**
	 * Sets the generation.
	 *
	 * @param generation the new generation
	 */
	public void setGeneration(int generation) {
		this.generation = generation;
	}

	/**
	 * Gets the generation.
	 *
	 * @return the generation
	 */
	public int getGeneration() {
		return this.generation;
	}

	/**
	 * Next generation.
	 */
	public void nextGeneration() {
		Cell[][] tempGrid = new Cell[this.getCols()][this.getRows()];

		for (int i = 0; i < this.getCols(); i++) {
			for (int j = 0; j < this.getRows(); j++) {
				tempGrid[i][j] = new Cell(this.calculateNewCellState(i, j, this.getAliveNeighboursCount(i, j)));
			}
		}
		for (int i = 0; i < this.getCols(); i++) {
			for (int j = 0; j < this.getRows(); j++) {
				grid[i][j].setState(tempGrid[i][j].getState());
			}
		}
		this.setGeneration(this.getGeneration() + 1);
	}

	/**
	 * Checks if is alive.
	 *
	 * @param row the row
	 * @param col the col
	 * @return true, if is alive
	 */
	public boolean isAlive(int col, int row) {
		return col >= 0 && row >= 0 && col < this.cols && row < this.rows && this.grid[col][row].getState();
	}

	/**
	 * Gets the alive neighbours count.
	 *
	 * @param col the col
	 * @param row the row
	 * @return the alive neighbours count
	 */
	public int getAliveNeighboursCount(int col, int row) {
		int aliveNeighboursCount = 0;
		for (int[] offset : NEIGHBOURS) {
			int neighborCol = col + offset[1];
			int neighborRow = row + offset[0];

			if (this.isAlive(neighborCol, neighborRow)) {
				aliveNeighboursCount++;
			}
		}
		return aliveNeighboursCount;
	}

	/**
	 * Calculate new cell state.
	 *
	 * @param col                  the col
	 * @param row                  the row
	 * @param aliveNeighboursCount the alive neighbours count
	 * @return true, if successful
	 */
	public boolean calculateNewCellState(int col, int row, int aliveNeighboursCount) {
		if (col < 0 || row < 0 || col >= this.cols || row >= this.rows) {
			return false;
		}

		Cell cell = this.getCell(row, col);
		if (!cell.getState() && aliveNeighboursCount == 3) {
			return true;
		}

		if (cell.getState() && (aliveNeighboursCount < 2 || aliveNeighboursCount > 3)) {
			return false;
		}

		return cell.getState();
	}

	/**
	 * Reset.
	 */
	public void reset() {
		this.initGrid(this.getCols(), this.getRows());
	}

	/**
	 * Randomize.
	 */
	public void randomize() {
		Random generator = new Random();
		for (int i = 0; i < this.getCols(); i++) {
			for (int j = 0; j < this.getRows(); j++) {
				this.grid[i][j].setState(generator.nextBoolean());
			}
		}
		this.setGeneration(0);
	}

	/**
	 * Inits the grid.
	 *
	 * @param cols the cols
	 * @param rows the rows
	 */
	private void initGrid(int cols, int rows) {
		this.setCols(cols);
		this.setRows(rows);
		this.setGeneration(0);

		this.grid = new Cell[this.getCols()][this.getRows()];

		for (int i = 0; i < this.getCols(); i++) {
			for (int j = 0; j < this.getRows(); j++) {
				this.grid[i][j] = new Cell(false);
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String matrix = "";
		for (int i = 0; i < this.getRows(); i++) {
			for (int j = 0; j < this.getCols(); j++) {
				matrix = matrix + (this.grid[j][i].getState() ? Cell.ALIVE : Cell.DEAD);
			}
			matrix = matrix + "\n";
		}
		//matrix = matrix+"\n";
		return matrix;
	}

	/**
	 * Gets the cell.
	 *
	 * @param row the row
	 * @param col the col
	 * @return the cell
	 */
	public Cell getCell(int row, int col) {
		return this.grid[col][row];
	}

	/**
	 * Load grid.
	 *
	 * @param file the file
	 */
	public void loadGrid(File file) {
		String line;
		Vector<String> fileGrid = new Vector<>();
		try {
			BufferedReader fileReader = new BufferedReader(new FileReader(file));
			while ((line = fileReader.readLine()) != null) {
				fileGrid.add(line);
			}
			fileReader.close();
		} catch (Exception e) {
		}

		this.setCols(fileGrid.get(0).length());
		this.setRows(fileGrid.size());
		this.setGeneration(0);

		this.grid = new Cell[this.getCols()][this.getRows()];

		for (int i = 0; i < this.getCols(); i++) {
			for (int j = 0; j < this.getRows(); j++) {
				String fieldValue = fileGrid.elementAt(j).substring(i, i + 1);
				boolean state = (fieldValue.equals(Cell.ALIVE) ? true : false);
				this.grid[i][j] = new Cell(state);
			}
		}
	}

	/**
	 * Save grid.
	 *
	 * @param file the file
	 */
	public void saveGrid(File file) {
		try {
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file));
			for (int i = 0; i < this.getRows(); i++) {
				for (int j = 0; j < this.getCols(); j++) {
					fileWriter.write((this.grid[j][i].getState()) ? Cell.ALIVE : Cell.DEAD);
				}
				fileWriter.newLine();
			}
			fileWriter.flush();
			fileWriter.close();
		} catch (Exception e) {
		}
	}

	public boolean checkStable() {
		if (this.checkDead()) {
			return true;
		}

		if (this.checkLoops(10)) {
			return true;
		}

		return false;
	}

	public boolean checkDead() {
		for (int row = 0; row < this.grid.length; row++) {
			for (int col = 0; col < this.grid[row].length; col++) {
				if (this.grid[row][col].getState()) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean checkLoops(int limit) {
		return false;
	}

	public boolean filesAreEqual(File file1, File file2) {
		try {
			BufferedReader reader1 = new BufferedReader(new FileReader(file1));
			BufferedReader reader2 = new BufferedReader(new FileReader(file2));
			String line1 = reader1.readLine();
			String line2 = reader2.readLine();
			boolean areEqual = true;

			while (line1 != null || line2 != null) {
				if (line1 == null || line2 == null) {
					areEqual = false;
					break;
				} else if (!line1.equalsIgnoreCase(line2)) {
					areEqual = false;
					break;
				}
				line1 = reader1.readLine();
				line2 = reader2.readLine();
			}

			reader1.close();
			reader2.close();
			return areEqual;
		} catch (Exception e) {
			return false;
		}
	}
}
