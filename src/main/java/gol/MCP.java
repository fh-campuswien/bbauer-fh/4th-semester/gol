package main.java.gol;

public class MCP {

	/**
	 * The main method.
	 *
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		GameOfLifeGUI gui = new GameOfLifeGUI();
		gui.init();
		gui.run();
	}
}
