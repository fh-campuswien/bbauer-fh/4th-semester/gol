package main.java.gol;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class MyJButton extends JButton implements Observer {

	MyJButton(String x) {
		super(x);
		this.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
	}

	@Override
	public void update(Observable o, Object arg) {
		this.setBackground(((Cell) o).getState() ? Color.BLUE : Color.WHITE);
	}
}
