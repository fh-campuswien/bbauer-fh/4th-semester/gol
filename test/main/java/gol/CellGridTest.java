package main.java.gol;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CellGridTest {

	CellGrid cellGrid;

	@BeforeEach
	void setUp() {
		cellGrid = new CellGrid(10, 10);
		this.cellGrid.setCellState(5, 5, true);
		this.cellGrid.setCellState(5, 6, true);
		this.cellGrid.setCellState(5, 7, true);
	}

	@Test
	void testIfCellIsAlive() {
		assertTrue(this.cellGrid.isAlive(5, 5), "Should be true if cell is alive");
		assertFalse(this.cellGrid.isAlive(1, 1), "Should be false if cell is dead");
	}

	@Test
	void testAliveNeighboursCount() {
		assertEquals(1, this.cellGrid.getAliveNeighboursCount(4, 4), "Cell should have 1 neighbor (in a corner)");
		assertEquals(1, this.cellGrid.getAliveNeighboursCount(5, 4), "Cell should have 1 neighbor (below it)");
		assertEquals(2, this.cellGrid.getAliveNeighboursCount(5, 6), "Cell should have 2 neighbors");
		assertEquals(3, this.cellGrid.getAliveNeighboursCount(4, 6), "Cell should have 3 neighbors");
	}

	@Test
	void testAliveInNeighbouringCellsInCorners() {
		cellGrid = new CellGrid(10, 10);
		this.cellGrid.setCellState(1, 1, true);
		this.cellGrid.setCellState(3, 1, true);
		this.cellGrid.setCellState(1, 3, true);
		this.cellGrid.setCellState(3, 3, true);
		assertEquals(4, this.cellGrid.getAliveNeighboursCount(2, 2), "Cell should have 4 neighbors in corners");
	}

	@Test
	void testAliveInNeighbouringCellsHorizontalAndVertical() {
		cellGrid = new CellGrid(10, 10);
		this.cellGrid.setCellState(2, 1, true);
		this.cellGrid.setCellState(1, 2, true);
		this.cellGrid.setCellState(3, 2, true);
		this.cellGrid.setCellState(2, 3, true);
		assertEquals(4, this.cellGrid.getAliveNeighboursCount(2, 2), "Cell should have 4 neighbors vertically and horizontally aligned");
	}

	@Test
	void testIfCellsWillComeAlive() {
		assertTrue(this.cellGrid.calculateNewCellState(1, 1, 3), "Cell should come alive if it has 3 neighbours");
	}

	@Test
	void testIfCellsWillDie() {
		assertFalse(this.cellGrid.calculateNewCellState(1, 1, 1), "Cell should die because if underpopulation");
		assertFalse(this.cellGrid.calculateNewCellState(5, 5, 4), "Cell should die because of overpopulation");
	}

	@Test
	void testIfCellsKeepTheirState() {
		this.cellGrid.setCellState(1, 1, true);
		assertTrue(this.cellGrid.calculateNewCellState(1, 1, 2), "Cell should stay alive");

		this.cellGrid.setCellState(1, 1, true);
		assertTrue(this.cellGrid.calculateNewCellState(1, 1, 3), "Cell should stay alive");

		this.cellGrid.setCellState(1, 1, false);
		assertFalse(this.cellGrid.calculateNewCellState(1, 1, 2), "Cell should stay dead");
	}
}